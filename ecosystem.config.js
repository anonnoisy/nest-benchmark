module.exports = {
  apps: [
    {
      name: 'nest-benchmark',
      script: './dist/main.js',
      exec_mode: 'cluster',
      instances: 1,
      kill_timeout: 4000,
      wait_ready: true,
      autorestart: true,
      log_date_format: 'YYYY-MM-DD HH:mm Z',
    },
  ],
};
