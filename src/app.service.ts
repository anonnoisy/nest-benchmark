import { Injectable } from '@nestjs/common';
import { AspNetUsers } from '@prisma/client';
import { PrismaService } from './prisma.service';

@Injectable()
export class AppService {
  constructor(private readonly prisma: PrismaService) {}

  async getUsers(): Promise<AspNetUsers[]> {
    return await this.prisma.aspNetUsers.findMany();
  }
}
