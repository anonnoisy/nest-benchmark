import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { AspNetUsers } from '@prisma/client';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('users')
  async getUsers(): Promise<AspNetUsers[]> {
    return await this.appService.getUsers();
  }
}
